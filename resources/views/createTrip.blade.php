@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <h1>Create a trip</h1>
          </div>
          <form action="{{route('trip.create')}}" method="post">
            {{csrf_field()}}
            Places number: <input type="text" name="placesNumber"><br>
            Departure city: <input type="text" name="departureCity"><br>
            Arrival city: <input type="text" name="arrivalCity"><br>
            Departure date: <input type="date" name="departureDate"><br>
            Price: <input type="text" name="price"><br>
            <button type="submit">Send</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
