@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <h1>Hello Users!</h1>
          </div>
          <h2>Here is the list of your users :)</h2>
          <div class="flex">
            <div class="flex-div dark">ID</div>
            <div class="flex-div dark">NAME</div>
            <div class="flex-div dark">EMAIL</div>
          </div>
          @foreach ($users as $user)
          <div class="flex">
              <div class="flex-div clear">{{ $user->id }}</div>
              <div class="flex-div clear">{{ $user->name }}</div>
              <div class="flex-div clear">{{ $user->email }}</div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection
