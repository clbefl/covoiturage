@extends('layouts.app')

@section('content')
<style>
  h1{
    text-align: center;
    color: orange;
    margin-bottom: 40px;
  }
  h2{
    text-align: center;
    color: #9abfd3;
    margin-bottom: 40px;
  }
  .flex{
    display: flex;
    justify-content: space-around;
    font-size: 25px;
    padding-top: 2px;
  }
  .dark{
    color: #FFFFFF;
    background-color: #5a6e79;
  }
  .clear{
    background-color: #9abfd3;
  }
  .flex-div{
    text-align: center;
    width: 33%;
    border-bottom: 1px solid #5a6e79;
  }
</style>
<form method="post" action="/trips/search">
	@csrf
	Departure city : <input type="text" name="departure" placeholder="Departure city">
	Arrival city : <input type="text" name="arrival" placeholder="Arrival city">
	<button type="submit" class="btn btn-primary">Find trips !</button>
</form>
@if ( Session::has('success') )
  <div class="alert-success">
    {{ Session::get('success') }}
  </div>
@endif
<div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h1>Hello User!</h1>
          </div>
          <h2>Trips :</h2>
          <div class="flex dark">
            <div class="flex-div">ID</div>
            <div class="flex-div">DRIVER_ID</div>
            <div class="flex-div">PLACES</div>
            <div class="flex-div">DEPARTURE CITY</div>
            <div class="flex-div">ARRIVAL CITY</div>
            <div class="flex-div">DEPARTURE DATE</div>
            <div class="flex-div">PRICE</div>
            <div class="flex-div">PLACES LEFT</div>
            <div class="flex-div"></div>
          </div>
          @foreach ($trips as $trip)
          <div class="flex clear">
              <div class="flex-div">{{ $trip->id }}</div>
              <div class="flex-div">{{ $trip->driver_id }}</div>
              <div class="flex-div">{{ $trip->places_number }}</div>
              <div class="flex-div">{{ $trip->departure_city }}</div>
              <div class="flex-div">{{ $trip->arrival_city }}</div>
              <div class="flex-div">{{ $trip->departure_date }}</div>
              <div class="flex-div">{{ $trip->price }}</div>
              <div class="flex-div">xx</div>
              <div class="flex-div">
                <form method="post" action="{{route('purpose', $trip->id)}}">
                  @csrf
                  <button type="submit" class="btn btn-primary">I want it!</button>
                </form>
              </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
  @endsection