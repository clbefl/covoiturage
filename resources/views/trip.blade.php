<style>
  h1{
    text-align: center;
    color: orange;
    margin-bottom: 40px;
  }
  h2{
    text-align: center;
    color: #9abfd3;
    margin-bottom: 40px;
  }
  .flex{
    display: flex;
    justify-content: space-around;
    font-size: 25px;
    padding-top: 2px;
  }
  .dark{
    color: #FFFFFF;
    background-color: #5a6e79;
  }
  .clear{
    background-color: #9abfd3;
  }
  .flex-div{
    text-align: center;
    width: 33%;
    border-bottom: 1px solid #5a6e79;
  }
</style>
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <a href="{{route('trips')}}" type="button">Return to trips</a>
        <div class="card">
          <h2>Here are trips that correspond to your research :</h2>
          <div class="flex dark">
            <div class="flex-div">ID</div>
            <div class="flex-div">DRIVER_ID</div>
            <div class="flex-div">PLACES</div>
            <div class="flex-div">DEPARTURE CITY</div>
            <div class="flex-div">ARRIVAL CITY</div>
            <div class="flex-div">DEPARTURE DATE</div>
            <div class="flex-div">PRICE</div>
          </div>
          @foreach ($trips as $trip)
          <div class="flex clear">
              <div class="flex-div">{{ $trip->id }}</div>
              <div class="flex-div">{{ $trip->driver_id }}</div>
              <div class="flex-div">{{ $trip->places_number }}</div>
              <div class="flex-div">{{ $trip->departure_city }}</div>
              <div class="flex-div">{{ $trip->arrival_city }}</div>
              <div class="flex-div">{{ $trip->departure_date }}</div>
              <div class="flex-div">{{ $trip->price }}</div>
            </div>
            <br>
        </div>
        @endforeach
      </div>
    </div>
  </div>