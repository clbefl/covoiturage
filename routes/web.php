<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Basic pages
 */
Route::get('/', 'HomeController@index')->name('home');

/**
 * Users
 */
Route::prefix('users')->group(function () {
    Route::get('/', 'UserController@showAll')->middleware('role:admin')->name('users');
    Route::get('/{id}', 'UserController@show')->name('user');
});

/**
 * Trips
 */
Route::prefix('trips')->group(function(){
	Route::get('/', 'TripController@index')->name('trips');
    Route::get('/create', 'TripController@createForm');
    Route::get('/{id}', 'TripController@show');
    Route::post('/search', 'TripController@search');
	Route::post('/create', 'TripController@create')->name('trip.create');
});

Route::prefix('proposals')->group(function () {
    Route::post('/', 'ProposalController@index')->name('purpose');
});

/**
 * Auth routes
 */
Auth::routes();
