<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TripsTableSeeder::class);
        $this->call(ProposalsTableSeeder::class);
        $this->command->info('✔ Seed successful');
    }
}
