<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1000)->create();

        /**
         * Create user
         */
        DB::table('users')->insert([
            'name' => 'test',
            'email' => 'test@test.test',
            'password' => bcrypt('test'),
            'car' => 'my car',
            'role' => null,
            'remember_token' => str_random(10)
        ]);

        /**
         * Create admin user
         */
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.admin',
            'password' => bcrypt('admin'),
            'car' => 'my car',
            'role' => 'admin',
            'remember_token' => str_random(10)
        ]);
    }
}
