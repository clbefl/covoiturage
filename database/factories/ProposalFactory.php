<?php

use Faker\Generator as Faker;

$factory->define(App\Proposal::class, function (Faker $faker) {
    return [
        'trip_id' => $faker->numberBetween(1, 50),
        'passenger_id' => $faker->numberBetween(1, 1000),
        'places_number' => $faker->numberBetween(1, 4),
        'accepted' => $faker->boolean,
        'note' => $faker->numberBetween(1, 5),
        'comment' => $faker->text,
    ];
});
