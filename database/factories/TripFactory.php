<?php

use Faker\Generator as Faker;

$factory->define(App\Trip::class, function (Faker $faker) {
    return [
        'driver_id' => rand(1, 10),
        'places_number' => rand(1, 4),
        'departure_city' => $faker->city,
        'arrival_city' => $faker->city,
        'departure_date' => $faker->date(),
        'price' => $faker->randomFloat('2', 1, 30)
    ];
});
