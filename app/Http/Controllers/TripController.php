<?php

namespace App\Http\Controllers;

use App\Trip;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $departure = $request->departure;
        $arrival = $request->arrival;
        if($departure && $arrival)
            $travels = DB::table('trips')->where('departure_city', $departure)->where('arrival_city', $arrival)->get();
        else if($departure && !$arrival)
            $travels = DB::table('trips')->where('departure_city', $departure)->get();
        else if(!$departure && $arrival)
            $travels = DB::table('trips')->where('arrival_city', $arrival)->get();
        else
            $travels = DB::table('trips')->get();
        return view('trip',  ['trips' => $travels]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hometrip', ['trips' => Trip::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $trip = new Trip([
            'driver_id' => Auth::user()->id,
            'places_number' => $request->placesNumber,
            'departure_city' => $request->departureCity,
            'arrival_city' => $request->arrivalCity,
            'departure_date' => Carbon::createFromFormat('Y-m-d', $request->departureDate),
            'price' => $request->price,
        ]);
        $trip->save();
        return redirect('trips')->with(['success' => 'Trip created']);
    }

    public function createForm()
    {
        return view('createTrip');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function show($trip)
    {
        return view('trip', ['trips' => Trip::find($trip)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function edit(Trip $trip)
    {
        //return view('trip', ['trip' => Trip::get($trip)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trip $trip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trip $trip)
    {
        //
    }
}
