<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver_id', 'places_number', 'departure_city','arrival_city','departure_date','price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function proposals()
    {
        return $this->hasMany('App\Proposal');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}